package com.pandemic;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class StreamPerformanceTester {

    private static final int MAX_SIZE = 9000000;
    private static final String NON_STREAM = "nonStream";
    private static final String STREAM = "stream";
    private static final List<Integer> numbers =  IntStream.range(0, MAX_SIZE * 2).boxed().collect(Collectors.toList());

    private final static Callable<List<Integer>> streamCallable = () -> numbers.stream()
            .filter(n -> n % 2 == 0)
            .map(n -> n * n)
            .limit(MAX_SIZE)
            .collect(toList());

    private final static Callable<List<Integer>> nonStreamCallable = () -> {
        List<Integer> evenSquares = new ArrayList<>();
        for (Integer number : numbers) {
            if (number % 2 == 0) {
                evenSquares.add(number * number);
                if (evenSquares.size() == MAX_SIZE) {
                    return evenSquares;
                }
            }
        }
        return evenSquares;
    };

    private String sequence;

    public static void main(String[] args) throws Exception {

        StreamPerformanceTester streamPerformanceTester = new StreamPerformanceTester();
        streamPerformanceTester.setSequence();

        Map<String, Callable<List<Integer>>> callableMapWithName = new HashMap<>();
        callableMapWithName.put(NON_STREAM, nonStreamCallable);
        callableMapWithName.put(STREAM, streamCallable);

        streamPerformanceTester.runCallableWithName(callableMapWithName);

    }

    private void setSequence() {
        System.out.println("Run test with:");
        System.out.println("[1] stream first");
        System.out.println("[2] non stream first");

        Scanner scanner=new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            if(input.equals("1")){
                this.sequence =  STREAM;
                return;
            }
            if(input.equals("2")){
                this.sequence = NON_STREAM;
                return;
            }
            System.out.println("enter the option 1 or 2");
        }
    }


    private void runCallableWithName(Map<String, Callable<List<Integer>>> callableWithName) throws Exception {
        checkTimeOnCallable(sequence, callableWithName.get(sequence));
        callableWithName.remove(sequence);
        Map.Entry<String, Callable<List<Integer>>> next = callableWithName.entrySet().iterator().next();
        checkTimeOnCallable(next.getKey(), next.getValue());
    }

    private void checkTimeOnCallable(String easyName, Callable<List<Integer>> listCallable) throws Exception {
        System.out.println("---------------------------------------------------------------------");
        System.out.println(easyName);
        long before = System.currentTimeMillis();
        List<Integer> evenSquares = listCallable.call();
        long after = System.currentTimeMillis();
        System.out.print("Milliseconds taken to perform: ");
        System.out.println(after - before);
        System.out.printf("Number of even squares computed: %d", evenSquares.size());
        System.out.println();
        System.out.println("---------------------------------------------------------------------");
    }


}
