# README #

### Resources ###

http://www.oracle.com/technetwork/articles/java/ma14-java-se-8-streams-2177646.html

### Java 8 Stream Profiling ###

```
|---------------------------------------------------------------------|---------------------------------------------------------------------|
|Run test with:                                                       |Run test with:                                                       |
|[1] stream first                                                     |[1] stream first                                                     |
|[2] non stream first                                                 |[2] non stream first                                                 |
|1                                                                    |2                                                                    |
|---------------------------------------------------------------------|---------------------------------------------------------------------|
|stream                                                               |nonStream                                                            |
|Milliseconds taken to perform: 459                                   |Milliseconds taken to perform: 357                                   |
|Number of even squares computed: 9000000                             |Number of even squares computed: 9000000                             |
|---------------------------------------------------------------------|---------------------------------------------------------------------|
|nonStream                                                            |stream                                                               |
|Milliseconds taken to perform: 182                                   |Milliseconds taken to perform: 281                                   |
|Number of even squares computed: 9000000                             |Number of even squares computed: 9000000                             |
|---------------------------------------------------------------------|---------------------------------------------------------------------|
```